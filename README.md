# NotesHUD

## Intro
In it's most basic form this is a head placed device that displays text sent 
from a mobile phone via bluetooth. 

## Hardware
This section discribes how we invision the hardware

### Optics

Generate a video using [blender](blender.org) or other software on a scanned
piece of paper to show the concept. 

### Electronics

The device needs to collect information from phone and display it. 


## Software

Type in text and send it over bluetooth. Currently information is going one way, 
from phone to device. Later we will work on sending information from device to 
phone. 

### Existing solutions

* [Gadget bridge app.](https://f-droid.org/wiki/page/nodomain.freeyourgadget.gadgetbridge#Description)
We want simple as possible. Is the information going in the right direction
* Needs to be compatible with our licence. 
* 

### New solutions

* design your own from scratch. make sure that it is easy expandable for future 
developments.