#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>            // a library that was included in the library 

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset. You can also connect this to arduino reset pin but you have to set it to -1 here.
#define TFT_DC     8
#define TFT_SCLK   6  // set these to be whatever pins you like!
#define TFT_MOSI   7


#define Black           0x0000      /*   0,   0,   0 */
#define Navy            0x000F      /*   l. 0,   0, 128 */
#define DarkGreen       0x03E0      /*   0, 128,   0 */
#define DarkCyan        0x03EF      /*   0, 128, 128 */
#define Maroon          0x7800      /* 128,   0,   0 */
#define Purple          0x780F      /* 128,   0, 128 */
#define Olive           0x7BE0      /* 128, 128,   0 */
#define LightGrey       0xC618      /* 192, 192, 192 */
#define DarkGrey        0x7BEF      /* 128, 128, 128 */
#define Blue            0x001F      /*   0,   0, 255 */
#define Green           0x07E0      /*   0, 255,   0 */
#define Cyan            0x07FF      /*   0, 255, 255 */  //Making life easier by defining colours
#define Red             0xF800      /* 255,   0,   0 */
#define Magenta         0xF81F      /* 255,   0, 255 */
#define Yellow          0xFFE0      /* 255, 255,   0 */
#define White           0xFFFF      /* 255, 255, 255 */
#define Orange          0xFD20      /* 255, 165,   0 */
#define GreenYellow     0xAFE5      /* 173, 255,  47 */
#define Pink                        0xF81F
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

tft.widht = uint16_t width();     // store height and width values to properties of the tft object
tft.height = uint16_t height();

void testdrawtext(char *text, uint16_t color, float  fontSize) {
  tft.setCursor(30,30);
  tft.setTextColor(color);
  tft.setTextWrap(true);
  tft.setTextSize(fontSize);
  tft.print(text);
}
void setup() {
  // put your setup code here, to run once:
  void setRotation(1); //Rotates the screen values 0 and 2 are protrait, values 1 and 3 are landscape
  tft.initR(INITR_BLACKTAB);
  tft.fillScreen(Black);
}

void loop() {
  // put your main code here, to run repeatedly:
  testdrawtext("Hello World!", Cyan, 2.5);
}




